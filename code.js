const historyBox = document.getElementById("last_operation_history");
const box = document.getElementById("box");
let calc = false;
const opArth = "+-/*";

function button_number(char) {
  if (char === "=") {
    egal();
  } else if (opArth.includes(char)) {
    historyArchive(char);
  } else {
    ajtChar(char);
  }
}

function ajtChar(char) {
  if (box.innerText === "zéro" || !calc) {
    box.innerText = char;
  } else {
    box.innerText += char;
  }

  calc = true;
}
function egal() {
  const lastChar = historyBox.innerText.charAt(historyBox.innerText.length - 1);
  if (!opArth.includes(lastChar)) {
    calc = false;
  }
  if (box.innerText !== "zéro" && calc) {
    historyBox.innerText += " " + box.innerText;
    box.innerText = eval(historyBox.innerText);
  }
  calc = false;
}
function historyArchive(char) {
  if (box.innerText !== "zéro" || historyBox.innerText) {
    const lastChar = historyBox.innerText.charAt(
      historyBox.innerText.length - 1
    );
    if (opArth.includes(lastChar)) {
      calc = false;
    }
    if (calc) {
      historyBox.innerText =
        eval(`${historyBox.innerText} ${char} ${box.innerText}`) + " " + char;
      box.innerText = eval(
        historyBox.innerText.slice(0, historyBox.innerText.length - 1)
      );
    } else {
      historyBox.innerText = eval(` ${box.innerText}`) + " " + char;
    }
    calc = false;
  }
}

function plus_minus() {
  box.innerText = eval(-box.innerText);
}
function clear_entry() {
  box.innerText = "zéro";
}
function button_clear() {
  historyBox.innerText = "";
  box.innerText = "zéro";
}

function backspace_remove() {
  if (box.innerText !== "zéro") {
    box.innerText = box.innerText.slice(0, box.innerText.length - 1);

    if (box.innerText.length === 0) {
      box.innerText = "zéro";
    }
  }
}
function square_root() {
  box.innerText = Math.sqrt(eval(box.innerText));
}
function power_of() {
  box.innerText = Math.pow(eval(box.innerText), 2);
}
function division_one() {
  box.innerText = eval(1 / eval(box.innerText));
}
function calculate_percentage() {
  return;
}
